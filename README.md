# Mullvad Connectivity Checker

[![devDependency Status](https://david-dm.org/nitrohorse/mullvad-connectivity-checker/dev-status.svg)](https://david-dm.org/nitrohorse/mullvad-connectivity-checker?type=dev)
[![Standard Style](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![License](https://img.shields.io/badge/license-GPLv3-yellow.svg)](https://github.com/nitrohorse/mullvad-connectivity-checker/blob/master/LICENSE)
[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgithub.com%2Fnitrohorse%2Fmullvad-connectivity-checker.svg?type=shield)](https://app.fossa.io/projects/git%2Bgithub.com%2Fnitrohorse%2Fmullvad-connectivity-checker?ref=badge_shield)
[![Downloads](https://img.shields.io/amo/users/mullvad-connectivity-checker.svg)](https://addons.mozilla.org/en-US/firefox/addon/mullvad-connectivity-checker/)
[![Say Thanks!](https://img.shields.io/badge/Say%20Thanks-!-1EAEDB.svg)](https://saythanks.io/to/nitrohorse)

Browser extension that checks every minute that you are connected to a Mullvad VPN server or not. Powered by [Mullvad](https://am.i.mullvad.net/api). Two checkmarks if you're connected through the [SOCKS5 proxy](https://mullvad.net/en/guides/socks5-proxy/), one checkmark if not.

![alt-text](https://i.imgur.com/XSOgj74.png)

![alt-text](https://i.imgur.com/n2Ij9b5.png)

## Download
* Firefox: https://addons.mozilla.org/en-US/firefox/addon/mullvad-connectivity-checker/

## Install Locally
* Firefox: [temporary](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Temporary_Installation_in_Firefox)
* Chrome: [permanent](https://superuser.com/questions/247651/how-does-one-install-an-extension-for-chrome-browser-from-the-local-file-system/247654#247654)

## Develop Locally
* Clone the repo
* Install tools:
	* [Node.js](https://nodejs.org/en/)
	* [yarn](https://yarnpkg.com/en/)
* Install dependencies: 
	* `yarn`
* Run add-on in isolated Firefox instance using [web-ext](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext):
	* `yarn start`
* Package for distribution:
	* `yarn bundle`
* More script commands can be found in the [package.json](https://github.com/nitrohorse/mullvad-connectivity-checker/blob/master/package.json)...


## License
[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgithub.com%2Fnitrohorse%2Fmullvad-connectivity-checker.svg?type=large)](https://app.fossa.io/projects/git%2Bgithub.com%2Fnitrohorse%2Fmullvad-connectivity-checker?ref=badge_large)
